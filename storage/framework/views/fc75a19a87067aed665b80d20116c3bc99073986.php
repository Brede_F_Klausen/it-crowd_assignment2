<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          
            <div class="panel panel-default">
                <div class="panel-heading">Create new topic</div>

                <div class="panel-body">
                    <?php echo Form::open(['route' => ['forums.categories.topics.store', $forumId, $categoryId]]); ?>

                          <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                              <?php echo Form::label('title', 'Title'); ?>

                              <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

                              <small class="text-danger"><?php echo e($errors->first('title')); ?></small>
                          </div>  

                          <div class="form-group<?php echo e($errors->has('body') ? ' has-error' : ''); ?>">
                              <?php echo Form::label('body', 'Body'); ?>

                              <?php echo Form::textarea('body', null, ['class' => 'form-control ckeditor']); ?>

                              <small class="text-danger"><?php echo e($errors->first('body')); ?></small>
                          </div>

                          <div class="form-group">
                            <div class="g-recaptcha<?php echo e($errors->has('g-recaptcha-response') ? ' has-error' : ''); ?>" data-sitekey="<?php echo e(env('GOOGLE_RECAPTCHA_KEY')); ?>">
                            </div>
                            <?php if($errors->has('g-recaptcha-response')): ?>
                                <span class="help-block"> 
                                    <small class="text-danger"><?php echo e($errors->first('g-recaptcha-response')); ?></small>
                                </span>
                            <?php endif; ?>
                          </div> 

                          <?php echo Form::submit('Create new topic', ['class'=>'btn btn-primary btn-block']); ?>

        
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
  <script src="<?php echo e(url('js/ckeditor/ckeditor.js')); ?>"></script>
  <script src="<?php echo e(url('js/ckeditor/config.js')); ?>"></script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>