<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <a href="<?php echo e(route('home.create_topic')); ?>" class="btn btn-default pull-right">Create new topic</a>
          <br>
          <br>
        </div>
        
        <div class="col-md-2">
          <div class="list-group">
            <a class="list-group-item active">Forum</a>
            <?php if(count($forums) == null): ?>
              <a href="#" class="list-group-item">Have no forum</a>
            <?php endif; ?>
            <?php $__currentLoopData = $forums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $forum): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
              <a href="<?php echo e(route('forums.show', $forum)); ?>" class="list-group-item"><?php echo e($forum->name); ?></a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
          </div>
        </div>
        <div class="col-md-10">
          

          <div class="panel panel-default">
            <div class="panel-heading">
              <b>Recent Post</b><span class="pull-right glyphicon glyphicon-pushpin"></span>
            </div>

            <?php if(count($topics) == null): ?>
                <ul class="list-group"> 
                  <a href="#" class="list-group-item">
                    Have no post
                  </a>
                </ul>
            <?php endif; ?>

            <?php $__currentLoopData = $topics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $topic): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <ul class="list-group">
                  <a href="<?php echo e(route('topics.show', $topic->slug)); ?>" class="list-group-item" style="padding:10px 1px">
                    <div class="col-md-10 col-xs-9">
                      <?php echo e($topic->title); ?> <br>
                      <?php if(count($topic->ratings)): ?>
                        <?php for($i = 0; $i < $topic->averageRating ; $i++): ?>
                          <i class="glyphicon glyphicon-star" style="color:#f6e729;"></i>
                        <?php endfor; ?>
                      <?php endif; ?>
                    </div>
                    <p style="font-size:12px;margin-top:2px;" class="">
                      <span class="fa fa-comments"></span> : <?php echo e(count($topic->comments)); ?> Replies <br>
                      <span class="glyphicon glyphicon-eye-open"></span> : <?php echo e($topic->views); ?> Views
                    </p>
                  </a>
                </ul>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
          </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>