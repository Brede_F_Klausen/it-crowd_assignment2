<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          
            <div class="panel panel-default">
                <div class="panel-heading">Create new topic</div>

                <div class="panel-body">
                    <?php echo Form::open(['route' => 'home.store_topic']); ?>

                          <div class="form-group<?php echo e($errors->has('category_id') ? ' has-error' : ''); ?>">
                              <?php echo Form::label('category_id', 'Categories'); ?>

                              <?php echo Form::select('category_id', $data, null, ['id' => 'category_id', 'class' => 'form-control', 'required' => 'required']); ?>

                              <small class="text-danger"><?php echo e($errors->first('category_id')); ?></small>
                          </div>

                          <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                              <?php echo Form::label('title', 'Title'); ?>

                              <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

                              <small class="text-danger"><?php echo e($errors->first('title')); ?></small>
                          </div>

                          <div class="form-group<?php echo e($errors->has('body') ? ' has-error' : ''); ?>">
                              <?php echo Form::label('body', 'Body'); ?>

                              <?php echo Form::textarea('body', null, ['class' => 'form-control ckeditor']); ?>

                              <small class="text-danger"><?php echo e($errors->first('body')); ?></small>
                          </div>

                          <div class="form-group">
                            <?php echo Recaptcha::render(); ?>

                            <?php if($errors->has('g-recaptcha-response')): ?>
                                <span class="help-block">
                                    <small class="text-danger"><?php echo e($errors->first('g-recaptcha-response')); ?></small>
                                </span>
                            <?php endif; ?>
                          </div> 

                          <?php echo Form::submit('Create new topic', ['class'=>'btn btn-primary btn-block']); ?>

        
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
  <script src="js/ckeditor/ckeditor.js"></script>
  <script src="js/ckeditor/config.js"></script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>