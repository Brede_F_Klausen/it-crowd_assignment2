# This project is tested and running on Ubuntu 18.04, it's not tested on other Unix-installations, but it should work when you get the right packages #

## To run the website after installation: ##

- php artisan serve --port=8080

## Login info for the admin account: ##

- email = admin@email.com
  password = password
  
## To create categories and forums: ##

- Admin/dashboard


# 1. Installation: #

** The first thing we need to do is install php and  it's dependencies **

` sudo apt-get install php libapache2-mod-php php-common php-mbstring php-xmlrpc php-soap php-gd php-xml php-mysql php-cli php-zip `

** After this you need to install sqlite for php. At the time of this writing the lastest version is 7.2, so replace the X with 2 **

`sudo apt-get install php7.X-sqlite3`

# 1.1 Note!!! #
**Version 7.2 of `php7.2-sqlite3`, however, includes a bug that will break the Messages feature in our site. To fix the bug do the following: **

After cloning the project to /home/youruser/, go into the folder you just cloned so that your working directory is something similar to this: `/home/youruser/it-crowd_assignment2`

cd into `vendor/laravel/framework/src/Illuminate/Database/Eloquent/` and open `Builder.php`

On line `1231` in the file, replace the code `originalWhereCount = count($query->wheres);` with `$originalWhereCount = is_array($query->wheres) ? count($query->wheres) : 0;`

Save the file and you're good to go!

# 2.0 Installing composer #

`sudo apt-get install composer`

# 3.0 install laravel #
From here you'll want to append your path to your bash environment. Run this command: `echo 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> ~/.bash_profile`

`composer global require "laravel/installer"` DO NOT RUN THIS COMMAND AS SUDO
# 4.0 Updating composer #

Change directory to the root of your project folder to something like this: `/home/youruser/it-crowd_assignment2`
And run this command: `composer update -vvvv` (this will take a while...)

# 5.0 Setting your environment #

[Go to the bitbucket site](https://bitbucket.org/Brede_F_Klausen/it-crowd_assignment2/) and find the `.env.example` file in the source. Click on this file and copy its contents.

In the projects root folder (`/home/youruser/it-crowd_assignment2`), create a new file and name it `.env` and paste the contents that you found in `.env.example`.

This step is very important to be able to make the DB connection and to register a new user and/or forum topic because it also enables Google-Captcha support.

Example: `nano .env`

# 6.0 Making the database ready #
Change diretory to the database folder like this: `/home/youruser/it-crowd_assignment2/database`

From here do a `nano database.sqlite`, save the file and exit (yes, its supposed to be empty)

# 7.0 Populating the DB #
Go back to the projects root folder: `/home/youruser/it-crowd_assignment2` and type in this command: `php artisan migrate --seed`

# 8.0 Adding your personal key to the environmental file #
In the projects root folder: `/home/youruser/it-crowd_assignment2` type `php artisan key:generate`

# After this you're ready to run the site: #
In the projects root folder: `/home/youruser/it-crowd_assignment2` type `php artisan serve --port=8080` and you should get a message saying `Laravel Development sever started on http://127.0.0.1:8080/`

However, due to Google dropping 127.0.0.1 for its Captcha service, make sure to use `localhost` instead of `127.0.0.1` when attempting to reach the site: `http://localhost:8080/`